<?php
/**
 * Created by PhpStorm.
 * User: ptibo
 * Date: 2018. 01. 22.
 * Time: 19:23
 */

namespace App\TwitterBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;

class JokesService extends BaseClient
{
    public function getJokes()
    {
        $messages = $this->query('/jokes/random/20');

        if (isset($messages->type) and $messages->type == 'success' ) {
            return$messages->value;
        } else {
            return array();
        }
    }
}