<?php
/**
 * Created by PhpStorm.
 * User: ptibo
 * Date: 2018. 01. 22.
 * Time: 19:23
 */

namespace App\TwitterBundle\Services;

use Doctrine\Common\Collections\ArrayCollection;

class TwitterService extends BaseClient
{
    public function getResultsByScreenName($name)
    {
        $query = "statuses/user_timeline.json?screen_name=".$name;
        return $this->query($query);
    }
}