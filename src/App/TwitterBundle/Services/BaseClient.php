<?php
/**
 * Created by PhpStorm.
 * User: ptibo
 * Date: 2018. 01. 22.
 * Time: 19:36
 */

namespace App\TwitterBundle\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Response;
use Symfony\Component\VarDumper\VarDumper;

class BaseClient
{
    /** @var  Client */
    protected $client;
    /** @var  Response */
    protected $response;

    /**
     * @param Client $client
     *
     * @return BaseClient
     */
    public function setClient($client)
    {
        $this->client = $client;

        return $this;
    }

    protected function query($url)
    {
        $messages = array();

        try {
            $this->response = $this->client->get($url);
            $messages = json_decode($this->response->getBody());
        } catch (ClientException $exception) {
            @error_log($exception->getMessage());
        }

        return $messages;
    }
}