<?php

namespace App\TwitterBundle\Controller;

use App\TwitterBundle\Manager\MessageManager;
use App\TwitterBundle\Services\JokesService;
use App\TwitterBundle\Services\TwitterService;
use Doctrine\Common\Collections\ArrayCollection;
use GuzzleHttp\Exception\ClientException;
use Mineur\TwitterStreamApi\Http\GuzzleStreamClient;
use Mineur\TwitterStreamApi\Model\Tweet;
use Mineur\TwitterStreamApi\PublicStream;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\VarDumper\VarDumper;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     * @return Response
     */
    public function indexAction()
    {
        return $this->render('@AppTwitter/index.html.twig');
    }

    /**
     * @Route("/{handle1}/{handle2}/{mod}")
     *
     * @param                $handle1
     * @param                $handle2
     * @param string         $mod
     *
     * @param TwitterService $twitterService
     * @param JokesService   $jokesService
     * @param MessageManager $messageManager
     *
     * @return Response
     */
    public function tweetsAction(
        $handle1, 
        $handle2, 
        $mod='mod',
        TwitterService $twitterService,
        JokesService $jokesService,
        MessageManager $messageManager    
    ) {
        if ($handle1 == $handle2) {
            return new Response('A két twitter user nem lehet ugyanaz!');
        }

        if (!in_array($mod, array('fib','mod'))) {
            return $this->redirectToRoute('app_twitter_default_tweets', array(
                'handle1' => $handle1,
                'handle2' => $handle2,
                'mod' => 'mod'
            ));
        }

        $results = $messageManager
            ->addTweets($twitterService->getResultsByScreenName($handle1))
            ->addTweets($twitterService->getResultsByScreenName($handle2))
            ->addJokes($jokesService->getJokes())
            ->mergeTweetsAndJokes($mod)
            ->getResults();


        return $this->render('@AppTwitter/table.html.twig', array(
            'data' => $results
        ));
    }

    /**
     * @param $handle
     *
     * @return mixed
     */


    protected function getChuckNorrisJokes()
    {
        $client = $this->get('guzzle.ichdb.client');

    }


}
