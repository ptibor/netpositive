<?php
/**
 * Created by PhpStorm.
 * User: ptibo
 * Date: 2018. 01. 22.
 * Time: 19:11
 */

namespace App\TwitterBundle\Manager;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Debug\Exception\ContextErrorException;
use Symfony\Component\VarDumper\VarDumper;

class MessageManager
{
    protected $tweets = array();
    protected $jokes = array();
    protected $results = array();
    protected $i = 1;

    /**
     * @param array $tweets
     *
     * @return $this
     */
    public function addTweets(array $tweets)
    {
        $this->tweets = array_merge($this->tweets, $tweets);
        $this->sortTweets();

        return $this;
    }

    /**
     * @return array
     */
    public function getTweets()
    {
        return $this->tweets;
    }

    /**
     * @return array
     */
    public function getJokes()
    {
        return $this->jokes;
    }

    public function addJokes(array $jokes)
    {
        $this->jokes = array_merge($this->jokes, $jokes);

        return $this;
    }

    public function getResults()
    {
        return $this->results;
    }

    public function sortTweets()
    {
        usort(
            $this->tweets,
            function ($a, $b) {
                $dateA = new \DateTime($a->created_at);
                $dateB = new \DateTime($b->created_at);

                return $dateB <=> $dateA;
            }
        );

        return $this;
    }

    public function mergeTweetsAndJokes($mod = 'mod')
    {
        $this->jokes = new ArrayCollection($this->jokes);

        $results = array();
        foreach ($this->tweets as $tweet) {
            if (
                ($mod == 'mod' and $this->i % 3 == 0) or
                ($mod == 'fib' and $this->i > 2 and $this->checkFibonacci($this->i))
            ) {
                $this->pushJoke();
            }

            array_push(
                $this->results,
                array(
                    'source'  => $tweet->user->name,
                    'time'    => $tweet->created_at,
                    'message' => $tweet->text,
                )
            );
            $this->i++;
        }

        return $this;
    }

    protected function checkFibonacci($n)
    {
        $dRes1 = sqrt((5 * pow($n, 2)) - 4);
        $nRes1 = (int)$dRes1;
        $dDecPoint1 = $dRes1 - $nRes1;
        $dRes2 = sqrt((5 * pow($n, 2)) + 4);
        $nRes2 = (int)$dRes2;
        $dDecPoint2 = $dRes2 - $nRes2;
        if (!$dDecPoint1 || !$dDecPoint2) {
            return true;
        } else {
            return false;
        }
    }

    protected function pushJoke()
    {
        try {
            array_push(
                $this->results,
                array(
                    'message' => $this->jokes->current()->joke,
                    'source'  => 'ICNDB',
                )
            );
            $this->jokes->next();
            $this->i++;
        } catch (ContextErrorException $e) {
            //
        }
    }
}