<?php
/**
 * Created by PhpStorm.
 * User: ptibo
 * Date: 2018. 01. 22.
 * Time: 20:34
 */

namespace App\TwitterBundle\Tests\Manager;


use App\TwitterBundle\Manager\MessageManager;
use Symfony\Bundle\FrameworkBundle\Tests\TestCase;

class MessageManagerTest extends TestCase
{
    var $messages = array();
    var $jokes = array();

    private function prepareMessages($i = 20) {
        for ($x = 0; $x <= $i; $x++) {
            $this->messages[] = (object) array(
                'created_at' => date('Y-m-d H:i:s', '10000'.$x),
                'text' => "Message #$x",
                'user' => (object) array('name' => 'Me')
            );
            $this->jokes[] = (object) array(
                'joke' => "Joke",
            );
        }
    }

    public function __construct($name = null, array $data = [], $dataName = '')
    {
        $this->prepareMessages();

        parent::__construct(
            $name,
            $data,
            $dataName
        );
    }

    public function testSorter() {
        $manager = new MessageManager();

        $shuffledMessages = $this->messages;
        shuffle($shuffledMessages);

        $manager->addTweets($shuffledMessages);
        $manager->sortTweets();
        $sortedMessages = $manager->getTweets();

        $this->assertEquals($sortedMessages, array_reverse($this->messages));
    }

    public function testMod() {
        $results = $this->getResults('mod');

        $threes = array(3,6,9,12,15,18,21,27);
        foreach($threes as $three) {
            $this->assertEquals($results[$three-1]['source'], 'ICNDB');
        }
    }

    public function testFib() {
        $this->prepareMessages(65);
        $results = $this->getResults('fib');

        // https://en.wikipedia.org/wiki/Fibonacci_number
        $numbers = array(3,5,8,13,21,34,55);
        foreach($numbers as $number) {
            $this->assertEquals($results[$number-1]['source'], 'ICNDB');
        }
    }

    /**
     * @return array
     */
    protected function getResults($mod)
    {
        $manager = new MessageManager();
        $manager->addTweets($this->messages);
        $manager->addJokes($this->jokes);
        $manager->mergeTweetsAndJokes($mod);
        $results = $manager->getResults();

        return $results;
    }
}